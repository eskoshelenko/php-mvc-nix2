<?php
$dsn = 'mysql:host=' . $_ENV['DB_HOST'] . ';';
$dsn .= 'port=' . $_ENV['DB_PORT'] . ';';
$dsn .= 'dbname=' . $_ENV['DB_DATABASE'];

return [
    'dsn'       => $dsn,
    'user_name' => $_ENV['DB_USERNAME'],
    'password'  => $_ENV['DB_PASSWORD'],
];
