<?php
namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\ArticleUserModel;
use Esko\Framework\Route;
use Esko\Framework\Session;

/**
 * Class ArticleController
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class ArticleController extends BaseController
{
    /**
     * @var ArticleModel
     */
    private ArticleModel $articleModel;
    /**
     * @var ArticleUserModel
     */
    private ArticleUserModel $articleUserModel;

    /**
     * ArticleController constructor
     * @return void
     */
    public function __construct()
    {
        $this->articleModel = new ArticleModel();
        $this->articleUserModel = new ArticleUserModel();
    }

    /**
     * Show articles list page
     * @return void
     */
    public function index(): void
    {
        $condition = ['users.id' => Session::get('id')];
        $articles = $this->articleModel->fullInfo($condition, 'sort');
        $data = [
            'pageTitle' => 'Articles list',
            'articles'  => $articles,
            'user'      => Session::get('email'),
            'article'   => Route::getUrl('article'),
            'new'       => Route::getUrl('articleNew'),
        ];

        $this->view('article/index', $data);
    }

    /**
     * Show article info page
     * @param array<string> $query
     * @return void
     */
    public function article(array $query): void
    {
        $id = $query['id'];
        $article = $this->articleModel->select(['id' => $id]);
        $data = [
            'pageTitle'   => 'Article info',
            'user'        => Session::get('email'),
        ];

        if ($article) {
            $data = array_merge($data, [
                'id'          => $id,
                'title'       => $article['title'],
                'description' => $article['description'],
                'edit'        => Route::getUrl('editArticle'),
                'delete'      => Route::getUrl('deleteArticle'),
            ]);
        } else {
            http_response_code(400);
        }

        $this->view($article ? 'article/article' : '404', $data);
    }

    /**
     * Show create new article page
     * @return void
     */
    public function new(): void
    {
        $data = [
            'pageTitle' => 'Create article',
            'user'      => Session::get('email'),
            'url'       => Route::getUrl('createArticle'),
            'redirect'  => Route::getUrl('articles'),
        ];

        $this->view('article/create', $data);
    }

    /**
     * Show edit articles page
     * @param array<string> $query
     * @return void
     */
    public function edit(array $query): void
    {
        $id = $query['id'];
        $article = $this->articleModel->select(['id' => $id]);
        $data = [
            'pageTitle'   => 'Edit article',
            'user'        => Session::get('email'),
        ];

        if ($article) {
            $data = array_merge($data, [
                'id'          => $id,
                'title'       => $article['title'],
                'description' => $article['description'],
                'url'         => Route::getUrl('updateArticle'),
                'redirect'    => Route::getUrl('articles'),
            ]);
        } else {
            http_response_code(400);
        }

        $this->view($article ? 'article/update' : '404', $data);
    }

    /**
     * Handle create article route
     * @return void
     */
    public function create(): void
    {
        $id = uniqueID();
        $_POST['id'] = $id;
        $userArticle = [
            'user_id' => Session::get('id'),
            'post_id' => $id,
        ];

        $this->articleUserModel->insert($userArticle);
        $this->articleModel->insert($_POST);
    }

    /**
     * Handle update article route
     * @return void
     */
    public function update(): void
    {
        foreach ($_POST as $column => $value) {
            if ($column === 'id') {
                $conditions[$column] = $value;
            } else {
                $columnsValues[$column] = $value;
            }
        }

        $this->articleModel->update($columnsValues, $conditions);
    }

    /**
     * Handle delete article route
     * @param array $query
     * @return void
     */
    public function delete($query): void
    {
        $conditions = [
            'post_id' => $query['id'],
            'user_id' => Session::get('id'),
        ];

        $this->articleUserModel->delete($conditions);
        $this->index();
    }
}
