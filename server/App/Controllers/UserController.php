<?php
namespace App\Controllers;

use App\Models\UserModel;
use Esko\Framework\Route;
use Esko\Framework\Session;

/**
 * Class UserController
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class UserController extends BaseController
{
    /**
     * @var UserModel
     */
    private UserModel $userModel;

    /**
     * UserController constructor
     * @return void
     */
    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    /**
     * Show user edit page
     * @return void
     */
    public function edit(): void
    {
        $data = [
            'pageTitle' => 'Edit user',
            'url'       => Route::getUrl('updateUser'),
            'id'        => Session::get('id'),
            'user'      => Session::get('email'),
            'redirect'  => Route::getUrl('editUser'),
            'delete'    => Route::getUrl('deleteUser'),
        ];

        $this->view('user/update', $data);
    }

    /**
     * Handle user create route
     * @return void
     */
    public function create(): void
    {
        $_POST['id'] = uniqueID();
        $_POST['password'] = hashPassword($_POST['password']);

        $this->userModel->insert($_POST);
    }

    /**
     * Handle user find route
     * @return void
     */
    public function find(): void
    {
        $_POST['password'] = hashPassword($_POST['password']);
        $user = $this->userModel->select($_POST);

        if ($user) {
            Session::set('id', $user['id']);
            Session::set('email', $user['email']);
            Session::set('gender_id', $user['gender_id']);
        } else {
            http_response_code(400);

            echo 'Wrong login or password';
        }
    }

    /**
     * Handle user update route
     * @return void
     */
    public function update(): void
    {
        if (isset($_POST['password'])) {
            $_POST['password'] = hashPassword($_POST['password']);
        }

        $conditions = [ 'id' => Session::get('id') ];

        $isUpdated = $this->userModel->update($_POST, $conditions);

        if ($isUpdated) {
            Session::set('email', $_POST['email']);
        }
    }

    /**
     * Handle delete user route
     * @return void
     */
    public function delete(): void
    {
        $conditions = [
            'id' => Session::get('id'),
            'email' => Session::get('email'),
        ];

        $isDeleted = $this->userModel->delete($conditions);

        if ($isDeleted) {
            Session::clear();
            header('Location: ' . $_ENV['APP_URL'] . Route::getUrl('login'));
        }
    }
}
