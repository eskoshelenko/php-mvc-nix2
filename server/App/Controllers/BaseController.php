<?php
namespace App\Controllers;

use Esko\Framework\Controllers\Controller;
use Esko\Framework\Session;

/**
 * Class BaseController
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class BaseController extends Controller
{
    /**
     * Render View with data
     * @param array|string $viewPath
     * @param array        $data
     * @return boolean
     */
    public function view(array|string $viewPath, array $data = []): bool
    {
        $head = 'layouts/head';
        $defaultHeader = 'layouts/headerDefault';
        $loggedHeader = 'layouts/headerLogged';
        $header = Session::has('id') ? $loggedHeader : $defaultHeader;
        $footer = 'layouts/footer';
        $viewPath = is_array($viewPath) ? $viewPath : [$viewPath];

        $viewPath = array_merge(
            [ $head, $header ],
            $viewPath,
            [ $footer ]
        );

        return parent::view($viewPath, $data);
    }
}
