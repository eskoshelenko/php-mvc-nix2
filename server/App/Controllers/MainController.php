<?php
namespace App\Controllers;

use Esko\Framework\Route;
use Esko\Framework\Session;

/**
 * Class MainController
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class MainController extends BaseController
{
    /**
     * Show default page
     * @return void
     */
    public function index(): void
    {
        $data = [ 'pageTitle' => 'Home' ];

        if (Session::has('email')) {
            $data['user'] = Session::get('email');
        }

        $this->view('index', $data);
    }

    /**
     * Show register page
     * @return void
     */
    public function register(): void
    {
        $data = [
            'pageTitle' => 'Register',
            'url'       => Route::getUrl('createUser'),
            'redirect'  => Route::getUrl('login'),
        ];

        $this->view('user/register', $data);
    }

    /**
     * Show login page
     * @return void
     */
    public function login(): void
    {
        $data = [
            'pageTitle' => 'Login',
            'url'       => Route::getUrl('findUser'),
            'redirect'  => Route::getUrl('home'),
        ];

        $this->view('user/login', $data);
    }

    /**
     * Handle logout route
     * @return void
     */
    public function logout(): void
    {
        Session::clear();
        header('Location: ' . $_ENV['APP_URL'] . Route::getUrl('login'));
    }
}
