<?php
namespace App\Models;

use Esko\Framework\Models\Model;

/**
 * Class ArticleModel
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class ArticleModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'posts';
    /**
     * @var string
     */
    protected string $users = 'users';
    /**
     * @var string
     */
    protected string $users_posts = 'users_posts';

    /**
     * Select articles from table
     * @param array  $condition
     * @param string $orderBy
     * @return string
     */
    public function fullInfo(array $condition, string $orderBy = 'sort'): string
    {
        $columns = [ 'posts.id as id', 'title' ];

        return $this->queryBuilder->select($this->table)
            ->cols($columns)
            ->join([$this->users_posts], ['id' => 'post_id'])
            ->join([$this->users_posts => $this->users], ['user_id' => 'id'])
            ->where($condition)
            ->order($orderBy)
            ->then(function ($error, $data) {
                if ($error) {
                    http_response_code(400);

                    return $error->getMessage();
                }

                http_response_code(200);
                $str = '';

                if (!empty($data)) {
                    foreach ($data as $article) {
                        $str .= '<li class="list-group-item"><a href="';
                        $str .= '{{article}}?id=' . $article['id'] . '">';
                        $str .= $article['title'] . '</a></li>';
                    }
                } else {
                    $str .= '<li class="list-group-item">There are no articles';
                    $str .= ' yet! <a href="{{new}}">Add new</a></li>';
                }

                $data = $str;

                return $data;
            });
    }
}
