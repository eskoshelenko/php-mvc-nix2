<?php
namespace App\Models;

use Esko\Framework\Models\Model;

/**
 * Class ArticleUserModel
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class ArticleUserModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users_posts';
}
