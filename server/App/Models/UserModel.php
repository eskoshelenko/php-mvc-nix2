<?php
namespace App\Models;

use Esko\Framework\Models\Model;

/**
 * Class UserModel
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class UserModel extends Model
{
    /**
     * @var string
     */
    protected string $table = 'users';
}
