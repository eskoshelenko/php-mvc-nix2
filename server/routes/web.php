<?php
use Esko\Framework\Route;
use App\Controllers\MainController;
use App\Controllers\ArticleController;
use App\Controllers\UserController;

$defaultRoute = '/';
$loginRoute = '/login';

Route::add($defaultRoute, [MainController::class, 'index'])
    ->bind('home');
Route::add('/register', [MainController::class, 'register'])
    ->bind('register');
Route::add($loginRoute, [MainController::class, 'login'])
    ->bind('login');
Route::add('/logout', [MainController::class, 'logout'])
    ->bind('logout');

// -------------------- UserRoutes -------------------
Route::add('/users/edit', [UserController::class, 'edit'])
    ->bind('editUser')
    ->dependsOn(isLoggedIn(), $loginRoute);
Route::add('/users/create', [UserController::class, 'create'])
    ->bind('createUser');
Route::add('/users/find', [UserController::class, 'find'])
    ->bind('findUser');
Route::add('/users/update', [UserController::class, 'update'])
    ->bind('updateUser')
    ->dependsOn(isLoggedIn(), $loginRoute);
Route::add('/users/delete', [UserController::class, 'delete'])
    ->bind('deleteUser')
    ->dependsOn(isLoggedIn(), $loginRoute);

// ------------------ ArticleRoutes ------------------
Route::add('/articles', [ArticleController::class, 'index'])
    ->bind('articles')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/?query', [ArticleController::class, 'article'])
    ->bind('article')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/new', [ArticleController::class, 'new'])
    ->bind('articleNew')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/update/?query', [ArticleController::class, 'edit'])
    ->bind('editArticle')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/create', [ArticleController::class, 'create'])
    ->bind('createArticle')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/update', [ArticleController::class, 'update'])
    ->bind('updateArticle')
    ->dependsOn(isLoggedIn(), $defaultRoute);
Route::add('/articles/delete/?query', [ArticleController::class, 'delete'])
    ->bind('deleteArticle')
    ->dependsOn(isLoggedIn(), $defaultRoute);
