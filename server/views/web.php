<?php
use Esko\Framework\Views\View;
use Esko\Framework\Route;

View::$path = ROOT . '/App/Views/';
View::$ext = '.html';

View::add('layouts/head');
View::add('layouts/headerDefault', [
    'home'  => Route::getUrl('home'),
    'login' => Route::getUrl('login'),
]);
View::add('layouts/headerLogged', [
    'home'     => Route::getUrl('home'),
    'articles' => Route::getUrl('articles'),
    'new'      => Route::getUrl('articleNew'),
    'editUser' => Route::getUrl('editUser'),
    'logout'   => Route::getUrl('logout'),
]);
View::add('index');
View::add('layouts/footer');
View::add('404');

View::add('user/register');
View::add('user/login');
View::add('user/update');

View::add('article/index');
View::add('article/article');
View::add('article/create');
View::add('article/update');
