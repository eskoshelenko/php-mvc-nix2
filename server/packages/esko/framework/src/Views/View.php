<?php
namespace Esko\Framework\Views;

/**
 * Class View
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class View
{
    /**
     * @var string
     */
    private static $injStart = '{{';
    /**
     * @var string
     */
    private static $injEnd = '}}';
    /**
     * @var array
     * [$key => $view]
     */
    private static array $views = [];
    /**
     * @var string
     */
    public static string $path = '';
    /**
     * @var string
     */
    public static string $ext = '';

    /**
     * Inserts data to view template
     * @param string $view
     * @param array  $data
     * @return string
     */
    private static function setData(string $view, array $data = []): string
    {
        foreach ($data as $key => $value) {
            $search = self::$injStart . $key . self::$injEnd;
            $view = str_replace($search, $value, $view);
        }

        return $view;
    }

    /**
     * Add view to views array by view as key
     * @param string $view
     * @param array  $data
     * @return void
     */
    public static function add(string $view, array $data = []): void
    {
        $fileView = self::$path . $view . self::$ext;

        if (is_file($fileView)) {
            $viewStr = file_get_contents($fileView);
            $viewStr = self::setData($viewStr, $data);

            self::$views[$view] = $viewStr;
        } else {
            echo '<h1>File' . $fileView . ' not found</h1>';
        }
    }

    /**
     * Render View with data
     * @param array $viewArr
     * @param array $data
     * @return void
     */
    public static function render(array $viewArr, array $data): void
    {
        $viewStr = '';

        foreach ($viewArr as $view) {
            $viewStr .= self::$views[$view];
        }

        $viewStr = self::setData($viewStr, $data);

        echo $viewStr;
    }
}
