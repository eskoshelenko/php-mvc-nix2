<?php
namespace Esko\Framework\Models;

use Esko\QueryBuilder\QueryBuilder;

/**
 * Class Model
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
abstract class Model
{
    /**
     * @var QueryBuilder
     */
    protected QueryBuilder $queryBuilder;
    /**
     * @var string
     */
    protected string $table;

    /**
     * Model constructor
     * @return void
     */
    public function __construct()
    {
        $this->queryBuilder = new QueryBuilder();
    }

    /**
     * Insert new data into table
     * @param array{string} $columnsValues
     * [column => value]
     * @return string
     */
    public function insert(array $columnsValues): string
    {
        $columnsValues = useHtmlspecialchars($columnsValues);

        return $this->queryBuilder->insert($this->table)
            ->cols(array_keys($columnsValues))
            ->vals(array_values($columnsValues))
            ->then(function ($error, $data) {
                if ($error) {
                    http_response_code(400);
                    echo $error->getMessage();

                    return false;
                } else {
                    http_response_code(201);

                    return true;
                }
            });
    }

    /**
     * Select data from table by condition
     * @param array{string} $conditions
     * [column => value]
     * @return array|null
     */
    public function select(array $conditions): array|null
    {
        $conditions = useHtmlspecialchars($conditions);

        return $this->queryBuilder->select($this->table)
            ->where([$conditions])
            ->then(function ($error, $data) {
                if ($error) {
                    http_response_code(400);

                    echo $error->getMessage();
                }
                $data = $data[0] ?? null;

                return $data;
            });
    }

    /**
     * Update data in table
     * @param array{string} $columnsValues
     * [column => value]
     * @param array{string} $conditions
     * [column => condition]
     * @return boolean
     */
    public function update(array $columnsValues, array $conditions): bool
    {
        $columnsValues = useHtmlspecialchars($columnsValues);
        $conditions = useHtmlspecialchars($conditions);

        return $this->queryBuilder->update($this->table)
            ->cols(array_keys($columnsValues))
            ->vals(array_values($columnsValues))
            ->where($conditions)
            ->then(function ($error, $id) {
                if ($error) {
                    http_response_code(400);

                    echo $error->getMessage();
                    return false;
                }

                return true;
            });
    }

    /**
     * Delete data from db by condition
     * @param array{string} $conditions
     * [column => condition]
     * @return boolean
     */
    public function delete(array $conditions): bool
    {
        $conditions = useHtmlspecialchars($conditions);

        return $this->queryBuilder->delete($this->table)
            ->where([$conditions])
            ->then(function ($error, $id) {
                if ($error) {
                    http_response_code(400);

                    echo $error->getMessage();

                    return false;
                }

                return true;
            });
    }
}
