<?php
namespace Esko\Framework;

use PDO;
use PDOException;
use Esko\Logger\Logger;

/**
 * Class DB
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class DB
{
    /**
     * @var object
     */
    protected object $pdo;
    /**
     * @var array
     */
    protected array $excl = [ 'password' ];
    /**
     * @var DB
     */
    protected static $instance;

    /**
     * DB constructor
     * @return void
     */
    protected function __construct()
    {
        $db = require ROOT . '/config/database.php';
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            // PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];

        $this->pdo = new PDO($db['dsn'], $db['user_name'], $db['password']);
    }

    /**
     * DB clone
     * @return void
     */
    protected function __clone()
    {}

    /**
     * Creates DB instance if it doesn't exists
     * @return DB
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Prepare and execute PDO statement with arguments
     * @param string            $sql
     * @param array<string|int> $args
     * @param callable          $callback
     * @return mixed
     */
    public function query(string $sql, array $args, callable $callback): mixed
    {
        Logger::log('sql:' . $sql . '|args:' . implode(',', $args));

        try {
            $stmt = $this->pdo->prepare($sql);
            $result = $stmt->execute($args);
            $data = [];

            if ($result !== false) {
                $count = 0;

                foreach ($stmt->fetchAll() as $row) {
                    foreach ($row as $key => $value) {
                        if (!is_numeric($key) && !in_array($key, $this->excl)) {
                            $data[$count][$key] = $value;
                        }
                    }

                    $count++;
                }
            }

            return $callback(null, $data);
        } catch (PDOException $e) {
            Logger::error('Error: ' . $e->getMessage());

            return $callback($e, null);
        }
    }
}
