<?php
namespace Esko\Framework;

/**
 * Class Route
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class Route
{
    /**
     * @var array<string>
     * [$controllerName, $controllerMethod]
     */
    protected static array $route = [];
    /**
     * @var array<array<string>>
     * [$url => [$controllerName, $controllerMethod]]
     */
    protected static array $routes = [];
    /**
     * @var string
     */
    private static string $url;
    /**
     * @var string
     */
    private static string $query;
    /**
     * @var array
     * [$bindName => $this->url]
     */
    protected static array $urls = [];
    /**
     * @var array
     * ['callback' => $callable, 'redirectTo' => $url]
     */
    protected static array $dependencies = [];

    /**
     * Parse url, update route query
     * @param string $url
     * @return string
     */
    private static function parseUrl(string $url): string
    {
        $urlAndQuery = explode('?', $url);
        $url = $urlAndQuery[0];
        self::$query = $urlAndQuery[1] ?? '';

        return $url;
    }

    /**
     * Add route to rutes by $url key
     * @param string        $url
     * @param array<string> $route
     * @return Route
     */
    public static function add(string $url, array $route = []): Route
    {
        $url = self::parseUrl($url);
        self::$url = $url;
        self::$routes[$url] = $route;

        return new self;
    }

    /**
     * Dispatch route by $url
     * @param string $url
     * @return void
     */
    public static function dispatch(string $url): void
    {
        $url = self::parseUrl($url);
        $tmp = $url;
        $url = self::checkRouteDependency($url);

        if (self::matchRoute($url)) {
            $controllerName = self::$route[0];

            if (class_exists($controllerName)) {
                $controllerInstance = new $controllerName();
                $controllerMethod = self::$route[1];

                if (method_exists($controllerInstance, $controllerMethod)) {
                    parse_str(self::$query, $query);
                    $query = !empty($query) ? $query : null;

                    if ($tmp !== $url) {
                        http_response_code(401);
                    }

                    $controllerInstance->$controllerMethod($query);
                } else {
                    echo 'Method ' . $controllerMethod . ' not implemented!';
                }
            } else {
                echo 'Controller ' . $controllerName . 'not found';
            }
        } else {
            http_response_code(404);

            include ROOT . '/App/Views/404.html';
        }
    }

    /**
     * Update route if $url exists in routes array
     * @param string $url
     * @return boolean
     */
    public static function matchRoute(string $url): bool
    {
        foreach (self::$routes as $urlKey => $route) {
            if ($urlKey == $url) {
                self::$route = $route;

                return true;
            }
        }

        return false;
    }

    /**
     * Get url by binding name
     * @param string $bindName
     * @return string
     */
    public static function getUrl(string $bindName): string
    {
        return self::$urls[$bindName];
    }

    /**
     * Bind current url by binding name
     * @param string $bindName
     * @return Route
     */
    public function bind(string $bindName): Route
    {
        self::$urls[$bindName] = self::$url;

        return new self;
    }

    /**
     * Add route dependency by callback
     * @param callable $callback
     * @param string   $redirectTo
     * @return Route
     */
    public function dependsOn(callable $callback, string $redirectTo) : Route
    {
        self::$dependencies[self::$url] = [
            'callback' => $callback,
            'redirectTo' => $redirectTo,
        ];

        return new self;
    }

    /**
     * Switch route if wrong dependency
     * @param string $url
     * @return string
     */
    public static function checkRouteDependency(string $url): string
    {
        if (isset(self::$dependencies[$url])) {
            $dependency = self::$dependencies[$url];
            $callback = $dependency['callback'];
            $redirectTo = $dependency['redirectTo'];

            return $callback() ? $url : $redirectTo;
        }

        return $url;
    }
}
