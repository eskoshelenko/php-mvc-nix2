<?php
use Esko\Framework\Session;

/**
 * Debug data
 * @param array<mixed> $array
 * @return void
 */
function dump($array): void
{
    echo '<pre>';
    var_dump($array);
    echo '</pre>';
}

/**
 * Debug data and die
 * @param array<mixed> $array
 * @return void
 */
function dd($array): void
{
    dump($array);
    die();
}

/**
 * Generate unique id
 * @param integer $length
 * @throws Exception throws error
 * @return string
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function uniqueID($length = 64): string
{
    if (function_exists('random_bytes')) {
        $bytes = random_bytes(ceil($length / 2));
    } elseif (function_exists('openssl_random_pseudo_bytes')) {
        $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
    } else {
        throw new Exception('no cryptographically secure random function available');
    }

    return substr(bin2hex($bytes), 0, $length);
}

/**
 * Check is user logged in
 * @return callable
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function isLoggedIn(): callable
{
    return function (): bool {
        return Session::has('id');
    };
}

/**
 * Hash password
 * @param string $password
 * @return string
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function hashPassword(string $password)
{
    return hash($_ENV['HASH_ALGO'], $password);
}

/**
 * Use htmlspecialchars to array keys, values
 * @param array $array
 * @return array
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function useHtmlspecialchars(array $array): array
{
    $newArray = [];

    foreach ($array as $key => $value) {
        $newArray[htmlspecialchars($key)] = htmlspecialchars($value);
    }

    return $newArray;
}
