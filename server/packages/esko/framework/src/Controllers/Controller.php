<?php
namespace Esko\Framework\Controllers;

use Esko\Framework\Views\View;

/**
 * Class Controller
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
abstract class Controller
{
    /**
     * Render View with data
     * @param array|string $viewPath
     * @param array        $data
     * @return boolean
     */
    public function view(array|string $viewPath, array $data = []): bool
    {
        View::render($viewPath, $data);
        return true;
    }
}
