<?php
namespace Esko\Framework;

/**
 * Class LocalStorage
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class LocalStorage
{
    /**
     * Set value in Local Storage by key
     * @param string         $key
     * @param string|integer $value
     * @return void
     */
    public static function set(string $key, string $value)
    {
        echo '<script>localStorage.setItem(\'' . $key . '\',\'' . $value . '\')</script>';
    }

    /**
     * Set value in Local Storage by key
     * @param string $key
     * @return string
     */
    public static function get(string $key): string
    {
        return '<script>localStorage.getItem(\'' . $key . '\')</script>';
    }

    /**
     * Clear Local Storage
     * @param string         $key
     * @param string|integer $value
     * @return void
     */
    public static function clear(string $key, string $value)
    {
        echo '<script>localStorage.clear()</script>';
    }
}
