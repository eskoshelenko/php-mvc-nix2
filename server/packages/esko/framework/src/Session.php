<?php
namespace Esko\Framework;

/**
 * Class Session
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class Session
{
    /**
     * Set value in session by key
     * @param string         $key
     * @param string|integer $value
     * @return void
     */
    public static function set(string $key, string|int $value): void
    {
        $_SESSION['storage'][$key] = $value;
    }

    /**
     * Get key value from session
     * @param string $key
     * @return string|integer|null
     */
    public static function get(string $key): string|int|null
    {
        return $_SESSION['storage'][$key];
    }

    /**
     * Check is session has key
     * @param string $key
     * @return boolean
     */
    public static function has(string $key): bool
    {
        if (!isset($_SESSION['storage'])) {
            $_SESSION['storage'] = [];
        }

        return array_key_exists($key, $_SESSION['storage']);
    }

    /**
     * Clear session data
     * @return void
     */
    public static function clear(): void
    {
        $_SESSION['storage'] = [];
    }
}
