<?php
namespace Esko\QueryBuilder\Cursors;

/**
 * Class QueryBuilder
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class UpdateCursorFactory extends Cursor
{
    /**
     * @var string
     */
    private string $columns = '';
    /**
     * @var array<string|int>
     */
    private array $args = [];
    /**
     * @var array<array<string|array<string>>>
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     */
    private array $whereClause = [];

    /**
     * Create columns part of statement
     * @param array $columns
     * @return UpdateCursorFactory
     */
    public function cols(array $columns): UpdateCursorFactory
    {
        foreach ($columns as $idx => $column) {
            $columns[$idx] = $column . '=?';
        }

        $this->columns = implode(',', $columns);

        return $this;
    }

    /**
     * Saving incoming values to args
     * @param array $values
     * @return UpdateCursorFactory
     */
    public function vals(array $values): UpdateCursorFactory
    {
        $this->args = [...$this->args, ...$values];

        return $this;
    }

    /**
     * Saving where statement and arguments
     * @param array<array<string|array<string>>> $conditions
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     * @return UpdateCursorFactory
     */
    public function where(array $conditions): UpdateCursorFactory
    {
        $this->whereClause = getStmtAndArgs($conditions);

        return $this;
    }

    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    public function then(callable $callback): mixed
    {
        $sql = 'UPDATE ' . $this->table;
        $sql .= ' SET ' . $this->columns;

        if (!empty($this->whereClause)) {
            $sql .= ' WHERE ' . $this->whereClause['statement'];
            $this->args = [...$this->args, ...$this->whereClause['arguments']];
        }

        $sql .= ';';

        return $this->database->query($sql, $this->args, $callback);
    }
}
