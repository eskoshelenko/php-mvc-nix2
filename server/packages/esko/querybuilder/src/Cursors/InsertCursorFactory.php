<?php
namespace Esko\QueryBuilder\Cursors;

use Exception;

/**
 * Class InsertCursorFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class InsertCursorFactory extends Cursor
{
    /**
     * @var string
     */
    private string $columns = '';
    /**
     * @var string
     */
    private string $values = '';
    /**
     * @var array<string|int>
     */
    private array $args = [];

    /**
     * Create columns part of statement
     * @param array $columns
     * @return InsertCursorFactory
     */
    public function cols(array $columns): InsertCursorFactory
    {
        $this->columns = ' ( ' .  implode(', ', $columns) . ' ) ';

        return $this;
    }

    /**
     * Saving incoming values to args, create values part of statement
     * @param array $values
     * @return InsertCursorFactory
     */
    public function vals(array $values): InsertCursorFactory
    {
        $this->args = $values;
        $this->values = '(' . substr(str_repeat('?,', count($values)), 0, -1) . ')';

        return $this;
    }


    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    public function then(callable $callback): mixed
    {
        if (empty($this->values)) {
            return $callback(new Exception('Empty vals'), null);
        } else {
            $sql = 'INSERT INTO ' . $this->table . $this->columns;
            $sql .= ' VALUES ' . $this->values . ';';

            return $this->database->query($sql, $this->args, $callback);
        }
    }
}
