<?php
namespace Esko\QueryBuilder\Cursors;

/**
 * Class DeleteCursorFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class DeleteCursorFactory extends Cursor
{
    /**
     * @var array<array<string|array<string>>>
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     */
    private array $whereClause = [];
    /**
     * @var array<string|int>
     */
    private array $args = [];

    /**
     * Saving where statement and arguments
     * @param array<array<string|array<string>>> $conditions
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     * @return DeleteCursorFactory
     */
    public function where(array $conditions): DeleteCursorFactory
    {
        $this->whereClause = getStmtAndArgs($conditions);
        return $this;
    }

    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    public function then(callable $callback): mixed
    {
        $sql = 'DELETE FROM ' . $this->table;

        if (!empty($this->whereClause)) {
            $sql .= ' WHERE ' . $this->whereClause['statement'];
            $this->args = $this->whereClause['arguments'];
        }

        $sql .= ';';

        return $this->database->query($sql, $this->args, $callback);
    }
}
