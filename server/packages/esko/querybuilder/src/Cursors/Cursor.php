<?php
namespace Esko\QueryBuilder\Cursors;

use Esko\Framework\DB;

/**
 * Parse condition, return associate array with operator, value keys
 * @param string $name
 * @param string $condition
 * @return array
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function parseCondition(string $name, string $condition): array
{
    $opeartors = [ '<=', '>=', '>', '<', '!=', '='];
    $length = count($opeartors);

    for ($i = 0; $i < $length; $i++) {
        if (str_starts_with($condition, $opeartors[$i])) {
            return [
                'operator' => $name . ' ' . $opeartors[$i] . '?',
                'value' => substr($condition, strlen($opeartors[$i]))
            ];
        }
    }

    if (str_contains($condition, '_') || str_contains($condition, '%')) {
        return [
            'operator' => $name . ' ' . 'LIKE ?',
            'value' => $condition
        ];
    }

    return [
        'operator' => $name . ' ' . '=?',
        'value' => $condition
    ];
}

/**
 * Get arguments and statement from condition
 * @param array $conditions
 * @return array
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
function getStmtAndArgs(array $conditions): array
{
    $statement = '';

    foreach ($conditions as $nameOR => $conditionOR) {
        if (is_array($conditionOR)) {
            foreach ($conditionOR as $nameAND => $conditionAND) {
                $parseCondition = parseCondition($nameAND, $conditionAND);
                $operatorsAND[] = $parseCondition['operator'];
                $arguments[] = $parseCondition['value'];
            }

            $operatorsOR[] = implode(' AND ', $operatorsAND);
        } else {
            $parseCondition = parseCondition($nameOR, $conditionOR);
            $operatorsOR[] = $parseCondition['operator'];
            $arguments[] = $parseCondition['value'];
        }
    }

    $statement .= implode(' OR ', $operatorsOR);

    return [
        'statement' => $statement,
        'arguments' => $arguments,
    ];
}

/**
 * Class Cursor
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
abstract class Cursor
{
    /**
     * @var DB
     */
    protected DB $database;
    /**
     * @var string
     */
    protected string $table;

    /**
     * Cursor constructor
     * @param DB     $database
     * @param string $table
     * @return void
     */
    public function __construct(DB $database, string $table)
    {
        $this->database = $database;
        $this->table = $table;
    }

    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    abstract public function then(callable $callback);
}
