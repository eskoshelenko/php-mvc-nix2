<?php
namespace Esko\QueryBuilder\Cursors;

use Exception;

/**
 * Class SelectCursorFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class SelectCursorFactory extends Cursor
{
    /**
     * @var string
     */
    private string $joinON = '';
    /**
     * @var array<array<string|array<string>>>
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     */
    private array $whereClause = [];
    /**
     * @var array<array<string|array<string>>>
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     */
    private array $havingClause = [];
    /**
     * @var array<string|int>
     */
    private array $args = [];
    /**
     * @var string
     */
    private string $columns = '*';
    /**
     * @var string
     */
    private string $orderBy = '';
    /**
     * @var string
     */
    private string $groupBy = '';
    /**
     * @var string
     */
    private string $limitBy = '';

    /**
     * Create columns part of statement
     * @param array $columns
     * @return SelectCursorFactory
     */
    public function cols(array $columns): SelectCursorFactory
    {
        $this->columns = implode(', ', $columns);

        return $this;
    }

    /**
     * Create join part of statement
     * @param array  $tables
     * [tableOne => tableTwo] | [tableTwo], tableOne = $this->table
     * @param array  $relations
     * [tableOneColumn => tableTwoColumn]
     * @param string $type
     * @return SelectCursorFactory
     */
    public function join(array $tables, array $relations, string $type = 'left'): SelectCursorFactory
    {
        foreach ($tables as $joinTableOne => $joinTableTwo) {
            $tableOne = $joinTableOne ? $joinTableOne : $this->table;
            $tableTwo = $joinTableTwo;
        }

        $this->joinON .= ' ' . strtoupper($type) . ' JOIN ' . $tableTwo . ' ON ';

        foreach ($relations as $columnOfTableOne => $columnOfTableTwo) {
            $onClause = $tableOne . '.' . $columnOfTableOne . ' = ';
            $onClause .= $tableTwo . '.' . $columnOfTableTwo;
            $tmpArr[] = $onClause;
        }

        $this->joinON .= implode(' AND ', $tmpArr) . ' ';

        return $this;
    }

    /**
     * Saving where statement and arguments
     * @param array<array<string|array<string>>> $conditions
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     * @return SelectCursorFactory
     */
    public function where(array $conditions): SelectCursorFactory
    {
        $this->whereClause = getStmtAndArgs($conditions);

        return $this;
    }

    /**
     * Create group by part of statement
     * @param string $columnName
     * @return SelectCursorFactory
     */
    public function group(string $columnName): SelectCursorFactory
    {
        $this->groupBy = ' GROUP BY ' . $columnName;

        return $this;
    }

    /**
     * Saving having statement and arguments
     * @param array<array<string|array<string>>> $conditions
     * [$name1 => $operator&Value1, [$name2 => $operator&Value2, ...], ...]
     * operator&Value: >=10
     * @return SelectCursorFactory
     */
    public function having(array $conditions): SelectCursorFactory
    {
        $this->havingClause = getStmtAndArgs($conditions);

        return $this;
    }

    /**
     * Create order by part of statement
     * @param string $columnName
     * @param string $order
     * @return SelectCursorFactory
     */
    public function order(string $columnName, string $order = 'ASC'): SelectCursorFactory
    {
        $this->orderBy = ' ORDER BY ' . $columnName . ($order === 'ASC' ? ' ASC' : ' DESC');

        return $this;
    }

    /**
     * Create limit part of statement
     * @param integer $limit
     * @param integer $offset
     * @return SelectCursorFactory
     */
    public function limit(int $limit, int $offset = 0): SelectCursorFactory
    {
        $this->limitBy = ' LIMIT ' . $limit . ($offset ? (' OFFSET ' . $offset) : '');

        return $this;
    }

    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    public function then(callable $callback): mixed
    {
        if (empty($this->groupBy) && !empty($this->havingClause)) {
            return $callback(new Exception('Using having without group by'), null);
        } else {
            $sql = 'SELECT ' . $this->columns;
            $sql .= ' FROM ' . $this->table;
            $sql .= $this->joinON;
            if (!empty($this->whereClause)) {
                $sql .= ' WHERE ' . $this->whereClause['statement'];
                $this->args = [...$this->args, ...$this->whereClause['arguments']];
            }
            $sql .= $this->groupBy;
            if (!empty($this->havingClause)) {
                $sql .= ' HAVING ' . $this->havingClause['statement'];
                $this->args = [...$this->args, ...$this->havingClause['arguments']];
            }
            $sql .= $this->orderBy;
            $sql .= $this->limitBy;
            $sql .= ';';

            return $this->database->query($sql, $this->args, $callback);
        }
    }
}
