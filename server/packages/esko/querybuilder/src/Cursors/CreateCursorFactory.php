<?php
namespace Esko\QueryBuilder\Cursors;

use Exception;

/**
 * Class CreateCursorFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class CreateCursorFactory extends Cursor
{
    /**
     * @var string
     */
    private string $tableSchema = '';

    /**
     * Create table schema statement
     * @param array $columnsAndTypes
     * @return CreateCursorFactory
     */
    public function schema(array $columnsAndTypes): CreateCursorFactory
    {
        foreach ($columnsAndTypes as $column => $type) {
            $this->tableSchema .= $column . ' ' . $type . ', ';
        }

        $this->tableSchema = substr($this->tableSchema, 0, -2);

        return $this;
    }

    /**
     * Prepeare statement and arguments, run database query, handle result by callback
     * @param callable $callback
     * @return mixed
     */
    public function then(callable $callback): mixed
    {
        if (empty($this->tableSchema)) {
            $callback(new Exception('Empty table schema'), null);
        } else {
            $sql = 'CREATE TABLE IF NOT EXISTS ' . $this->table . ' ( ';
            $sql .= $this->tableSchema . ');';

            return $this->database->query($sql, [], $callback);
        }
    }
}
