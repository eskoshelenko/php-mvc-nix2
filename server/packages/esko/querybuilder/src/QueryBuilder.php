<?php
namespace Esko\QueryBuilder;

use Esko\Framework\DB;

/**
 * Class QueryBuilder
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class QueryBuilder
{
    /**
     * Create instance of cursor factory by method
     * @param string $method
     * @param array  $table
     * @return mixed
     * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
     */
    public function __call(string $method, array $table): mixed
    {
        $cursor = 'Esko\\QueryBuilder\\Cursors\\' . ucfirst($method) . 'CursorFactory';

        if (class_exists($cursor)) {
            return new $cursor(DB::instance(), ...$table);
        } else {
            echo 'Method ' . ucfirst($method) . ' is not allowed!';
        }
    }
}
