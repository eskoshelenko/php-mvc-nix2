<?php
namespace Esko\Logger;

/**
 * Class Logger
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class Logger
{
    /**
     * Logging log level message to factory
     * @param string $message
     * @return void
     */
    public static function log(string $message): void
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $message);
    }

    /**
     * Logging info level message to factory
     * @param string $message
     * @return void
     */
    public static function info(string $message): void
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $message);
    }

    /**
     * Logging warning level message to factory
     * @param string $message
     * @return void
     */
    public static function warning(string $message): void
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $message);
    }

    /**
     * Logging error level message to factory
     * @param string $message
     * @return void
     */
    public static function error(string $message): void
    {
        self::method(require 'config/log.php')->writeLog(__FUNCTION__, $message);
    }

    /**
     * Chose logger by incoming class
     * @param string $class
     * @return object
     */
    public static function method(string $class): object
    {
        $className = 'Esko\\Logger\\Methods\\' . ucfirst($class) . 'LoggerFactory';

        if (class_exists($className)) {
            return $className::getInstance();
        } else {
            exit('This class does not exists ' . ucfirst($class) . 'LoggerFactory');
        }
    }
}
