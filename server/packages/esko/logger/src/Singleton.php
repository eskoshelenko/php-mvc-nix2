<?php
namespace Esko\Logger;

/**
 * Class Singleton
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class Singleton
{
    /**
     * @var array<object>
     */
    private static array $instances = [];

    /**
     * Singleton constructor
     * @return void
     */
    protected function __construct()
    {}

    /**
     * Singleton clone
     * @return void
     */
    protected function __clone()
    {}

    /**
     * Create istance of static class if doesn't exists
     * @return object
     */
    public static function getInstance(): object
    {
        $class = static::class;

        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new static();
        }

        return self::$instances[$class];
    }
}
