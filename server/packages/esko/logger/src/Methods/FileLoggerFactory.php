<?php
namespace Esko\Logger\Methods;

use Esko\Logger\Singleton;

/**
 * Class FileLoggerFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class FileLoggerFactory extends Singleton implements MethodInterfaceFactory
{
    /**
     * @var resource|false
     */
    private $handle;
    /**
     * @var string
     */
    private $separator = '|';

    /**
     * FileLoggerFactory constructor
     * @return void
     */
    protected function __construct()
    {
        $fileName = '/store/' . date('Y-m-d') . '.ext';
        $this->handle = fopen($_SERVER['DOCUMENT_ROOT'] . $fileName, 'a+');
    }

    /**
     * Writing log
     * @param string $level
     * @param string $message
     * @return void
     */
    public function writeLog(string $level, string $message): void
    {
        $instance = static::getInstance();
        $message = implode($instance->separator, [
            date('G:i:s'),
            $level,
            print_r($message, true)
        ]);

        fwrite($instance->handle, $message);
    }
}
