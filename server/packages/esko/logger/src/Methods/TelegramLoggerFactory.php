<?php
namespace Esko\Logger\Methods;

use Esko\Logger\Singleton;

/**
 * Class TelegramLoggerFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class TelegramLoggerFactory extends Singleton implements MethodInterfaceFactory
{
    /**
     * @var string
     */
    private string $url;
    /**
     * @var string
     */
    private string $separator = '|';
    /**
     * @var \CurlHandle|false
     */
    private $cURLConnection;

    /**
     * TelegramLoggerFactory constructor
     * @return void
     */
    protected function __construct()
    {
        $config = require '../config/telegram.php';
        $host = 'https://api.telegram.org';
        $token = $config['token'];
        $path = '/bot' . $token . '/sendMessage?';
        $chat_id = $config['chat_id'];

        $this->url = $host . $path . 'chat_id=' . $chat_id . '&text=';
        $this->cURLConnection = curl_init();
    }

    /**
     * Writing log
     * @param string $level
     * @param string $message
     * @return void
     */
    public function writeLog(string $level, string $message): void
    {
        $instance = static::getInstance();
        $text = implode($instance->separator, [
            date('Y-n-d G:i:s'),
            $level,
            print_r($message, true)
        ]);

        curl_setopt($instance->cURLConnection, CURLOPT_URL, $instance->url . $text);
        curl_exec($instance->cURLConnection);

        if (curl_error($instance->cURLConnection)) {
            echo curl_error($instance->cURLConnection);
        }
    }
}
