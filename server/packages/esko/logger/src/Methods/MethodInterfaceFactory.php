<?php
namespace Esko\Logger\Methods;

/**
 * Interface MethodInterfaceFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
interface MethodInterfaceFactory
{
    /**
     * Writing log
     * @param string $level
     * @param string $message
     * @return void
     */
    public function writeLog(string $level, string $message): void;
}
