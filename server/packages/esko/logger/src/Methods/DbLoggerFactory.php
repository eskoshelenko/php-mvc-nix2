<?php
namespace Esko\Logger\Methods;

use Esko\Logger\Singleton;
use Esko\Framework\DB;

/**
 * Class DbLoggerFactory
 * @author Eugeny Koshelenko <eskoshelenko@gmail.com>
 */
class DbLoggerFactory extends Singleton implements MethodInterfaceFactory
{
    /**
     * Writing log
     * @param string $level
     * @param string $message
     * @return void
     */
    public function writeLog(string $level, string $message): void
    {
        $textAndStatus = explode(',', $message);
        $text = $textAndStatus[0];
        $status = $textAndStatus[1] ?? null;
        $sql = 'INSERT INTO logs ( level, status_code, message )';
        $sql .= ' VALUES ( ?, ?, ? );';

        DB::instance()
            ->query($sql, [$level, $status, $text], function ($err, $data) {
                if ($err) {
                    echo $err->getMessage() . '<br>';
                }

                dump($data);
            });
    }
}
