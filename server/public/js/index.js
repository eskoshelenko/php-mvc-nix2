"use strict";
const info = document.getElementById('error');
function serialize(collection)
{
    let serialize = '';
    for (const { name, value } of collection) {
        if (name && name !== 'confirmPassword') {
            serialize += `${name}=${value}&`;
        }
    }
    return serialize.slice(0, -1);
}
function hasEmptyValues(collection)
{
    for (const { name, value } of collection) {
        if (name && !value) {
            return true;
        }
    }
    return false;
}
function showError(msg, type)
{
    return `<div class="alert alert-${type} text-center" role="alert">${msg}</div>`;
}
function submitHandler(e)
{
    const { elements, action, method, enctype, dataset } = e.target;
    const { password, confirmPassword } = elements;
    const options = {
        method,
        credentials: 'same-origin',
        headers: {
            'Content-Type': enctype || 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: `${serialize(elements)}${dataset.id ? `&id=${dataset.id}` : ''}`
    };
    e.preventDefault();
    if (hasEmptyValues(elements)) {
        info.innerHTML = showError('All fields are required', 'danger');
    }
    else {
        if (confirmPassword && password.value !== confirmPassword.value) {
            info.innerHTML = showError('Passwords didn\'t match', 'danger');
        }
        else {
            fetch(action, options)
                .then(async (res) => {
                    const data = await res.text();
                    if (res.status < 400) {
                        if (dataset.url) {
                            location.href = dataset.url;
                        } else {
                            info.innerHTML = showError('Success!', 'success');
                        }
                    }
                    else {
                        info.innerHTML = showError(data, 'danger');
                    }
                })
                .catch(console.log);
        }
    }
}
document.body.addEventListener('submit', submitHandler, true);