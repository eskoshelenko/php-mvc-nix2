<?php
require_once '../vendor/autoload.php';
require_once '../packages/esko/framework/src/function.php';

session_start();

define('URL', $_SERVER['REQUEST_URI']);
define('ROOT', dirname(__DIR__));

use Esko\Framework\Route;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

require_once ROOT . '/routes/web.php';
require_once ROOT . '/views/web.php';

Route::dispatch(URL);
