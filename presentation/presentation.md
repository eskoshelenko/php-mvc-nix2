# Realization

## First part

![db_schema](./img/db_schema.png)

Tables:
- [**users**](./database/users.sql)
- [**genders**](./database/genders.sql)
- [**articles**](./database/articles.sql)

Relations:
- [**users_articles**](./database/users_articles.sql)
- [**constraints**](./database/relations.sql)

## Second part

1. **Registration** 
![registration](./img/register.png)
2. **Autorization**
![authorization](./img/login.png)
3. **Personal account**
4. **Edit user**
5. **Delete user**
![personal account](./img/account.png)
6. **Create an article**
![create an article](./img/create.png)
7. **Show article**
8. **Delete article**
![show article](./img/article.png)
9. **Update the article**
![update the article](./img/edit.png)