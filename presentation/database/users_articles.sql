CREATE TABLE IF NOT EXISTS nix_2.users_posts (
  post_id  VARCHAR(64),
  user_id  VARCHAR(64),
  CONSTRAINT pkGroupPosts PRIMARY KEY (post_id, user_id)
);