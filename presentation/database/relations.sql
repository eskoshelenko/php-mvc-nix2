ALTER TABLE nix_2.users
ADD CONSTRAINT fkUsers 
FOREIGN KEY (gender_id) REFERENCES nix_2.genders (id);

ALTER TABLE nix_2.users
ADD CONSTRAINT chkUsersEmail 
CHECK (email LIKE '%@%.%');

ALTER TABLE nix_2.users_posts
ADD CONSTRAINT fkUsersPostsUserId 
FOREIGN KEY (user_id) REFERENCES nix_2.users (id) 
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE nix_2.posts
ADD CONSTRAINT fkUsersPostsPostId 
FOREIGN KEY (id) REFERENCES nix_2.users_posts (post_id) 
  ON DELETE CASCADE ON UPDATE CASCADE;