# Task **MVC framework**

## First part - Database

Desighn database:

1. Table **users**. Columns:
- id
-	email
-	password
-	created_at
-	updated_at

> the password must be encrypted

2. Table **gender**. Columns:
- id
- gender_name

> must be made according to ISO/IEC 5218

3. Table **articles**. Columns:
- id
-	title
-	description
-	sort
-	created_at
-	updated_at

Tables relations:
1. users - genders one to one
2. users - articles many to many

> If the user deletes his account, then the articles should be deleted too

## Second part - Application

Create registration form and personal account in which you can create an article

Should be implemented:
1. registration
2. authorization

3. personal account
4. edit user
5. delete user

6. create an article
7. delete article
8. update the article
9. show article

## [Go to presentation](./presentation/presentation.md)